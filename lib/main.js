'use strict';

document.querySelector('#button').addEventListener('click', dateSetter);

function dateSetter(){

  //Cambiar valor del boton
  let b = document.querySelector('#button')   
  b.classList.toggle("is-loading")    
  
  // Data print on table..
  setTimeout(() => {

    // toggle class is-hidden
    let inf = document.querySelector('#section')
    inf.classList.toggle("is-hidden") 


    fetch('https://jsonplaceholder.typicode.com/posts')
    .then(response => response.json())
    .then(dato => {Traer(dato)} )
  }, 600);

}



//Function that allow data pints
function Traer(dato) {   

  let res = document.querySelector('#info')

  res.innerHTML = ''

  for (let i of dato){
    res.innerHTML += `
      <tr>
        <th>${i.userId}</th>
        <th>${i.id}</th>
        <th>${i.title}</th>
        <th>${i.body}</th>
      </tr>
    `
  }  

  // Cambiar valor del atributo del boton
  let b = document.querySelector('#button')
  b.classList.toggle("is-loading")

  // Eliminate class is-hidden
  let inf = document.querySelector('#section')
  inf.className -= "is-hidden"
  

}





